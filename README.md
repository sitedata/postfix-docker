# Postfix in Docker

## Description

Pure [Postfix][postfix] installation in a Docker image based on the
[postfix package][postfix-package] in Arch Linux.
Fully customizable via Docker mounted volumes for usage on a Virtual Private Server (VPS).

## Docker repository and tags

See the [registry page][registry].

## Using the image

Note: The following examples require valid Postfix configuration files in `/etc/postfix`.
See the [Postfix documentation][postfix-doc] for more details.

### Run with Docker

```shell
docker run -it -p 25:25 \
  --volume=/etc/postfix:/etc/postfix \
  --volume=/var/spool/postfix:/var/spool/postfix \
  --volume=/var/log/postfix:/var/log/postfix \
  registry.gitlab.com/moasda/postfix-docker/postfix-docker:3.8.4-1-2023-12-29
```

### Run with Docker Compose

Example for `docker-compose.yml`:
```yaml
version: '3.8'

services:
  postfix:
    image: registry.gitlab.com/moasda/postfix-docker/postfix-docker:3.8.4-1-2023-12-29
    container_name: postfix
    restart: always
    ports:
      - "25:25"
    volumes:
      - /etc/postfix:/etc/postfix
      - /var/spool/postfix:/var/spool/postfix
      - /var/log/postfix.log:/var/log/postfix.log
```

### Run with Ansible

Example playbook file, including support for logrotate:
```yaml
---
- name: Install Postfix
  hosts: all
  tasks:
    # Copy config files to /etc/postfix ...

    - name: Postfix service
      community.docker.docker_container:
        name: postfix
        image: registry.gitlab.com/moasda/postfix-docker/postfix-docker:3.8.4-1-2023-12-29
        container_default_behavior: no_defaults
        restart_policy: always
        ports:
          - 25:25
        volumes:
          - '/etc/postfix:/etc/postfix'
          - '/var/spool/postfix:/var/spool/postfix'
          - '/var/log/postfix:/var/log/postfix'

    - name: Postfix logrotate
      ansible.builtin.blockinfile:
        path: /etc/logrotate.d/postfix
        block: |
          /var/log/postfix/postfix.log {
            monthly
            rotate 12
            compress
            delaycompress
            missingok
            notifempty
            create 644 root root
            postrotate
              docker kill --signal="USR1" postfix
            endscript
          }
        create: true
```

## Build Docker image

This project can create a new Docker image using the GitLab CI script in `.gitlab-ci.yml`.
The build is triggered by a new tag.

1. Create repository tag, e.g. `2023-12-29`
2. GitLab CI will create a new docker image named `postfix-docker:<postfix version>-<tag name>`<br>
   with `<postfix version>` = latest version of package `postfix` in Arch Linux<br>
   and  `<tag name>` = repository tag name from step 1<br>
   Example docker image name: `postfix-docker:3.8.4-1-2023-12-29`
3. Run the Docker image, see section above.

## License

The files in this repository are licensed under GPL-3.0-only &ndash; see the
[LICENSE](LICENSE) file for details.

[postfix]: https://www.postfix.org/
[postfix-doc]: https://www.postfix.org/documentation.html
[postfix-package]: https://archlinux.org/packages/extra/x86_64/postfix/
[registry]: https://gitlab.com/moasda/postfix-docker/container_registry
